import Head from 'next/head';
import '../styles/globals.scss';
import 'bootstrap/dist/css/bootstrap.css';
function MyApp({ Component, pageProps }) {
    return (
        <>
            <Head>
                <meta name='viewport' content='width=device-width, initial-scale=1' />
                <link rel='stylesheet' href='//cdn.jsdelivr.net/npm/hack-font@3.3.0/build/web/hack-subset.css'></link>
                <title>Nick Malkin</title>
            </Head>
            <Component {...pageProps} />
        </>
    );
}

export default MyApp;
