import Link from 'next/link';
import Sidebar from '../components/sidebar';

export default function Miscellaneous({ projectsJSON, musicJSON }) {
    return <Sidebar projects={projectsJSON} musics={musicJSON} />;
}

export async function getStaticProps() {
    //get projects from api
    const projects = await fetch('https://nick-malkin-backend.herokuapp.com/projects');
    const projectsJSON = await projects.json();
    //get music from api
    const music = await fetch('https://nick-malkin-backend.herokuapp.com/sounds');
    const musicJSON = await music.json();
    return {
        props: { projectsJSON, musicJSON },
    };
}
