import Sidebar from '../../components/sidebar';
import projectStyles from '../../styles/Project.module.scss';

export default function Project({ project, projectsJSON, musicsJSON }) {
    const replaceContent = (data) => {
        let content = data.replace(/href/g, "target='_blank' href");
        content = content.replace(/src="/g, `src="https://nick-malkin-backend.herokuapp.com/`);
        return content;
    };
    return (
        <div>
            <div className='row d-flex '>
                <div className={`col-xl-4 col-sm-12`}>
                    <Sidebar projects={projectsJSON} musics={musicsJSON} />
                </div>
                <div className={`col-xl-8 col-sm-12 d-flex justify-content-center ${projectStyles.main_project}`}>
                    <div className=''>
                        <div className={projectStyles.description}>{project.description}</div>
                        <div
                            className={projectStyles.content}
                            dangerouslySetInnerHTML={{
                                __html: replaceContent(project.content ? project.content : ''),
                            }}
                        />
                        <div className={projectStyles.pictures}>
                            {project.pictures &&
                                project.pictures.map((picture) => {
                                    return <img className={projectStyles.picture} key={picture.id} src={picture.url} />;
                                })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export async function getStaticPaths() {
    const res = await fetch(`https://nick-malkin-backend.herokuapp.com/projects`);
    const projects = await res.json();

    const paths = projects.map((project) => ({
        params: { slug: project.slug },
    }));

    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const { slug } = params;

    const res = await fetch(`https://nick-malkin-backend.herokuapp.com/projects?slug=${slug}`);
    const res1 = await fetch(`https://nick-malkin-backend.herokuapp.com/projects?_sort=release_date:desc`);
    const data = await res.json();
    const data1 = await res1.json();

    const project = data[0];
    const projectsJSON = data1;

    //get music from api
    const musics = await fetch('https://nick-malkin-backend.herokuapp.com/sounds?_sort=release_date:desc');
    const musicsJSON = await musics.json();

    return {
        props: { project, projectsJSON, musicsJSON },
    };
}
