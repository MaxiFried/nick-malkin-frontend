import dynamic from 'next/dynamic';
import Link from 'next/link';
import Sidebar from '../../components/sidebar';
import musicStyles from '../../styles/Music.module.scss';

export default function Music({ music, musicsJSON, projectsJSON }) {
    const BandcampIframe = dynamic(() => import('../../components/bandcampIframe'));
    const replaceContent = (data) => {
        let content = data.replace(/href/g, "target='_blank' href");
        content = content.replace(/src="/g, `src="https://nick-malkin-backend.herokuapp.com/`);
        return content;
    };
    return (
        <>
            <div className='row'>
                <div className={`col-xl-4 col-sm-12`}>
                    <Sidebar projects={projectsJSON} musics={musicsJSON} />
                </div>
                <div className={`col-xl-8 col-sm-12 d-flex justify-content-center ${musicStyles.main_music}`}>
                    <div className={musicStyles.content}>
                        <BandcampIframe src={music.bandcamp.src} href={music.bandcamp.href} />
                        {/* <h2 className={musicStyles.title}>{music.title}</h2> */}
                        <div>{music.description}</div>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: replaceContent(music.content ? music.content : ''),
                            }}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export async function getStaticPaths() {
    const res = await fetch(`https://nick-malkin-backend.herokuapp.com/sounds`);
    const musics = await res.json();

    const paths = musics.map((music) => ({
        params: { slug: music.slug },
    }));

    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const { slug } = params;

    const res = await fetch(`https://nick-malkin-backend.herokuapp.com/sounds?slug=${slug}`);
    const res1 = await fetch(`https://nick-malkin-backend.herokuapp.com/sounds?_sort=release_date:desc`);
    const data = await res.json();
    const data1 = await res1.json();

    const music = data[0];
    const musicsJSON = data1;

    //get projects from api
    const projects = await fetch('https://nick-malkin-backend.herokuapp.com/projects?_sort=release_date:desc');
    const projectsJSON = await projects.json();

    return {
        props: { music, musicsJSON, projectsJSON },
    };
}
