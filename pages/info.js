import Sidebar from '../components/sidebar';
import infoStyles from '../styles/Info.module.scss';

export default function Info({ projectsJSON, musicJSON }) {
    return (
        <div>
            <div className='row d-flex '>
                <div className={`col-xl-4 col-sm-12`}>
                    <Sidebar projects={projectsJSON} musics={musicJSON} />
                </div>
                <div className={`col-xl-8 col-sm-12 d-flex justify-content-center ${infoStyles.main_info}`}>
                    <div className={infoStyles.content}>
                        <p>Nick Malkin</p>
                        <p>b. 1985, Princeton, NJ</p>
                        <p>lives and works in Los Angeles, CA</p>
                        <p> Inquiries: nicholascroziermalkin@gmail.com</p>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export async function getStaticProps() {
    //get projects from api
    const projects = await fetch('https://nick-malkin-backend.herokuapp.com/projects');
    const projectsJSON = await projects.json();
    //get music from api
    const music = await fetch('https://nick-malkin-backend.herokuapp.com/sounds');
    const musicJSON = await music.json();
    return {
        props: { projectsJSON, musicJSON },
    };
}
