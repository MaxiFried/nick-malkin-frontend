import Link from 'next/link';
import sidebarStyles from '../../styles/Sidebar.module.scss';

export default function Sidebar({ projects = [], musics = [] }) {
    // const [showProjects, setShowProjects] = useState(false);
    // const [showMusic, setShowMusic] = useState(false);
    // const handleClick = (menu) => {
    //     if (menu === 'projects') {
    //         setShowProjects(true);
    //         setShowMusic(false);
    //     } else if (menu === 'music') {
    //         setShowProjects(false);
    //         setShowMusic(true);
    //     } else {
    //         setShowProjects(false);
    //         setShowMusic(false);
    //     }
    // };

    return (
        <div className={sidebarStyles.sidebar_component}>
            {/* loop over prjects and show them  */}
            <div className={sidebarStyles.title}>
                <h1>Nick Malkin</h1>
            </div>
            <div className={sidebarStyles.subtitle}>
                <h2>Music</h2>
            </div>
            <ul className={sidebarStyles.items}>
                {musics &&
                    musics.map((music) => (
                        <li key={music.id}>
                            <Link href={`/music/${music.slug}`} key={music.id}>
                                <h3>{music.title}</h3>
                            </Link>
                        </li>
                    ))}
            </ul>

            <div className={sidebarStyles.subtitle}>
                <h2 className={sidebarStyles.bold}>Projects</h2>
            </div>
            <ul className={sidebarStyles.items}>
                {projects &&
                    projects.map((project) => (
                        <li key={project.id}>
                            <Link href={`/project/${project.slug}`} key={project.id}>
                                <h3>{project.title}</h3>
                            </Link>
                        </li>
                    ))}
            </ul>
            {/* <div className={sidebarStyles.subtitle}>
                <Link href={`/miscellaneous`}>
                    <h2>Miscellaneous</h2>
                </Link>
            </div> */}
            <div className={sidebarStyles.footer}>
                <Link href={`/info`}>
                    <h2>Info</h2>
                </Link>
            </div>
        </div>
    );
}
